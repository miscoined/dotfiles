;;; -*- lexical-binding: t -*-

;;; Constants
(defconst my-win-p (eq system-type 'windows-nt) "Non-nil if using MS Windows.")

(defconst my-state-dir
  (if my-win-p
      (expand-file-name "state" user-emacs-directory)
    (expand-file-name "emacs" (or (getenv "XDG_STATE_HOME") "~/.local/state")))
  "Directory for Emacs user state files.")

(defconst my-data-dir
  (if my-win-p
      (expand-file-name "data" user-emacs-directory)
    (expand-file-name "emacs" (or (getenv "XDG_DATA_HOME") "~/.local/share")))
  "Directory for Emacs user data files.")

(defconst my-cache-dir
  (if my-win-p
      (expand-file-name "cache" user-emacs-directory)
    (expand-file-name "emacs" (or (getenv "XDG_CACHE_HOME") "~/.cache")))
  "Directory for Emacs cache files.")

;; Edit configuration file
(defun my-edit-config ()
  "Edit personal Emacs configuration file."
  (interactive)
  (find-file user-init-file))


;;; Configuration management

;; straight.el for package management
;; use the development version specifically to resolve
;; https://github.com/radian-software/straight.el/issues/985
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(use-package straight
  :custom (straight-use-package-by-default t  "Use straight.el by default, no package.el here"))

(use-package use-package
  :custom
  (use-package-enable-imenu-support t "Generate imenu entries for use-package declarations")
  (use-package-verbose t              "Verbose use-package logging because debugging is hard"))

(use-package general :demand t			; Evil-friendly keybind configuration
  :config
  (general-auto-unbind-keys)			; Automatically unbind keys if necessary
  (general-evil-setup t))				; Set up short names for state maps

;;; Basic settings

;; TODO use doom-emacs startup screen
;; https://github.com/doomemacs/doomemacs/blob/master/modules/ui/doom-dashboard/config.el
(use-package emacs
  :hook (prog-mode . next-error-follow-minor-mode)
  :general ("C-j" #'visible-mode)
  :custom
  (ring-bell-function 'ignore      "No bell")
  (scroll-conservatively 101       "Never recenter when scrolling")
  (scroll-margin 3                 "Lines of context to keep in view when scrolling")
  (frame-title-format "%b "        "Buffer name as frame title")
  (echo-keystrokes 0.1             "Faster command echo")
  (delete-by-moving-to-trash t     "Avoid accidentally deleting things forever by using trash")
  (user-full-name "Kelly Stewart"  "Introduce myself to emacs")
  (display-fill-column-indicator t "Show the fill column visually")
  (display-fill-column-indicator-character ?│ "Use a nice unicode character for showing the column")
  (fill-column 100                 "70 characters wide is pretty tight, let's up it a little")
  (tab-width 4                     "8 character wide tabs is so wide. 4 is more reasonable.")
  (inhibit-startup-screen t                "No default startup screen")
  (initial-scratch-message nil             "No scratch message")
  (display-startup-echo-area-message "Welcome" "Nicer startup message")
  (initial-major-mode 'fundamental-mode    "Scratch in fundamental mode for faster loading")
  (auto-save-list-file-prefix (expand-file-name "autosaves/save-" my-state-dir)
                              "Keep autosaves away from the rest of my files")
  (user-mail-address "miscoined@gmail.com" "Don't try to guess my email")
  (save-interprogram-paste-before-kill t "Don't overwrite clipboard contents")
  (read-extended-command-predicate #'command-completion-default-include-p
                                   "Hide irrelevant commands when completing M-x")
  (auto-save-list-file-prefix (expand-file-name "autosaves/" my-state-dir)
                              "Keep autosaves away from the rest of my files")
  (cycle-spacing-actions '(just-one-space delete-all-space delete-space-after delete-space-before restore)
                         "Add more options to cycle spacing")
  (delete-active-region 'kill "Kill the active region when we delete it")
  (shell-command-prompt-show-cwd t "Show current directory when prompting for a shell command")
  (yank-pop-change-selection t "Rotating kill ring changes system clipboard")
  (kill-read-only-ok t "Don't signal an error for killing read-only text")
  (track-eol t "Moving between lines when at eol sticks the cursor to eol")
  (visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow)
                                 "Show fringe indicators when wrapping lines")
  (sentence-end-double-space nil "Sentences end with a single space when filling")
  :config
  (setq-default indent-tabs-mode nil)  	; In this house we use spaces
  (menu-bar-mode -1)                    ; disable menu bars
  (tool-bar-mode -1)                    ; disable toolbars
  (scroll-bar-mode -1)					; disable scrollbar
  (line-number-mode -1)        ; Don't need the line number in the modeline, I have line numbers on.
  (auto-save-mode t)           ; Periodically keep a safety save
  (auto-fill-mode t)           ; Automatically break lines at fill-col
  (global-visual-line-mode t)) ; Consider visual lines not logical lines


(use-package novice :custom (disabled-command-function nil "Don't disable any commands"))

;;; Evil

(use-package evil :demand t             ; Vim emulation
  :general
  ("<f3>" #'evil-record-macro)
  ('(motion normal)
   "SPC" nil
   "q" #'kill-this-buffer
   "\"" #'evil-jump-item
   "*" #'evil-use-register
   ;; using this instead of `evil-respect-visual-line-mode' to avoid
   ;; modifying dd behavior
   [remap evil-next-line] #'evil-next-visual-line
   [remap evil-previous-line] #'evil-previous-visual-line)
  ('(motion visual normal insert emacs)
   :prefix "SPC"
   :non-normal-prefix "M-SPC"
   :prefix-command 'my-leader-command
   :prefix-map 'my-leader-map
   "f" #'find-file
   "b" #'switch-to-buffer
   "w" #'save-buffer
   "SPC" #'execute-extended-command)
  :custom
  (evil-undo-system 'undo-fu      "Use undo-fu instead of builtin undo")
  (evil-want-Y-yank-to-eol t      "Make Y yank to eol")
  (evil-want-C-w-in-emacs-state t "Allow C-w commands even in emacs state")
  (evil-want-keybinding nil       "Disable keybinding so we can use evil-collection")
  (evil-cross-lines t             "Let f, t, and others move past the end of the line")
  (evil-echo-state nil            "I don't need another thing telling me what state I'm in")
  :config
  ;; Center the search result after using n to get the next search entry
  (define-advice center-next-search (:after nil evil-search-next)
    (evil-scroll-line-to-center nil))
  (evil-ex-define-cmd "q" #'kill-this-buffer)  ; :q to kill this buffer
  (evil-ex-define-cmd "quit" #'evil-quit)      ; :quit to actually quit
  (evil-mode t))

(use-package evil-collection            ; Predefined Evil bindings everywhere
  :after evil
  :config (evil-collection-init))

(use-package evil-escape :after evil    ; Easier escape key
  :hook (evil-mode . evil-escape-mode))

(use-package evil-surround :after evil  ; Surround operator
  :hook (prog-mode . evil-surround-mode))

(use-package evil-matchit :after evil   ; Use jump to jump between tags as well as parens
  :hook (evil-mode . global-evil-matchit-mode)
  :general (evil-matchit-mode-map [remap evil-jump-item] #'evilmi-jump-items))

(use-package evil-goggles :after evil   ; Visual editing hints for evil
  :hook (evil-mode . evil-goggles-mode)
  ;; USe diff-mode faces for editing actions
  :config (evil-goggles-use-diff-faces))

;;; Help
(use-package help-mode :straight nil	; Built-in help
  :general
  (help-mode-map
   "H" #'help-go-back
   "L" #'help-go-forward))
(use-package which-key                  ; Auto-complete for keybinds
  :config
  (which-key-setup-minibuffer)
  (which-key-mode t))

(use-package which-func                 ; Function in modeline
  :hook (prog-mode . which-function-mode)
  :init
  (defun my-disable-which-func ()
    "Disable which-func mode in the current buffer."
    (interactive)
    (which-func-mode -1)))

;;; UI
(use-package time                       ; Built-in time in modeline
  :custom (display-time-day-and-date t "Show date as well as time")
  :config (display-time-mode t))

(use-package embark                     ; Like a minibuffer context menu
  :general
  ([remap describe-bindings] #'embark-bindings)
  ('(motion normal)
   "m" #'embark-act
   "M" #'embark-dwim))

(use-package frame :straight nil        ; Built-in frame management
  :config
  (unless my-win-p             ; auto raise and lower frames when on an OS where focus follows mouse
    (auto-raise-mode t)
    (auto-lower-mode t))
  (blink-cursor-mode -1))               ; don't blink the cursor

(use-package goto-addr                  ; Built-in URL buttonizing
  :hook
  (prog-mode . goto-address-prog-mode)
  (text-mode . goto-address-mode))

(use-package display-line-numbers       ; Line numbers
  :hook (prog-mode . display-line-numbers-mode)
  :custom
  (display-line-numbers-type 'relative "Relative line numbers and absolute number for current line")
  (display-line-numbers-widen t        "Disregard narrowing when showing line numbers"))

(use-package mwheel	:straight nil		; Built-in mousewheel scrolling
  :custom
  (mouse-wheel-scroll-amount '(3 ((shift) 3)) "Scroll 3 lines at a time")
  (mouse-wheel-progressive-amount t           "Accelerate scrolling")
  (mouse-wheel-follow-mouse t                 "Scroll the window under the mouse"))
(use-package anzu                       ; Show search matches in modeline
  :config
  (global-anzu-mode t))

(use-package evil-anzu                  ; Anzu for evil searches
  :after '(evil anzu))

(use-package saveplace                  ; Save place in file
  :custom
  (save-place-file (expand-file-name "places" my-state-dir))
  :config (save-place-mode t))

;;;; Minibuffer

(use-package minibuffer :straight nil	; Built-in minibuffer
  :custom (read-file-name-completion-ignore-case t "Ignore case when completing file names"))

(use-package vertico :demand t          ; Minibuffer completion
  :hook (after-init . vertico-mode)
  :general
  (vertico-map
   "C-j" #'vertico-next
   "C-k" #'vertico-previous
   "M-j" #'vertico-next-group
   "M-k" #'vertico-previous-group)
  (minibuffer-local-map
   "C-w" #'backward-kill-word)
  :custom
  (vertico-cycle t  "Enable cycling for next and previous commands")
  (vertico-resize t "Resize the window based on the number of candidates")
  :config
  ;; Prefix current candidate with a symbol.
  ;; https://github.com/minad/vertico/wiki#prefix-current-candidate-with-arrow
  (define-advice vertico--format-candidate
      (:around (orig-fun cand prefix suffix index _start) prefix-current-candidate)
    (let ((cand (funcall orig-fun cand prefix suffix index _start)))
      (concat (if (= vertico--index index) (propertize "» " 'face 'vertico-current) "  ") cand))))

(use-package vertico-repeat             ; Vertico resume last session
  :after vertico
  :straight nil
  :hook (minibuffer-setup-hook . vertico-repeat-save)
  :general (my-leader-map "C-SPC" #'vertico-repeat))

(use-package vertico-mouse              ; Vertico mouse support for completion
  :after vertico
  :straight nil
  :hook (vertico-mode . vertico-mouse-mode))

(use-package vertico-quick              ; Vertico avy-like selection
  :after vertico
  :straight nil
  :general (vertico-map
            "M-q" #'vertico-quick-insert
            "C-q" #'vertico-quick-exit))

(use-package vertico-multiform          ; Vertico multiple forms
  :after vertico
  :straight nil
  :general (vertico-map "M-g" #'my-vertico-multiform-cycle)
  :hook (vertico-mode . vertico-multiform-mode)
  :init
  (defun my-vertico-multiform-cycle ()
    "Cycle between vertico multiform forms"
    (interactive)
    (cond
     ((not (or vertico-flat-mode vertico-grid-mode))
      (vertico-multiform--temporary-mode 'vertico-flat-mode t))
     (vertico-flat-mode (progn (vertico-multiform--temporary-mode 'vertico-flat-mode -1)
                               (vertico-multiform--temporary-mode 'vertico-grid-mode t)))
     (vertico-grid-mode (progn (vertico-multiform--temporary-mode 'vertico-grid-mode -1)
                               (vertico-multiform--temporary-mode 'vertico-flat-mode -1))))))

(use-package vertico-indexed            ; Vertico indexed candidates
  :after vertico
  :straight nil
  :hook (vertico-mode . vertico-indexed-mode)
  :config
  ;; Bind M-1 through M-9 to `vertico-insert' for that indexed candidate
  (defmacro my-vertico-bind-indexed-insert-fun (num)
    "Bind M-`num' to the a function which executes `vertico-insert' with the the prefix arg"
    (let ((fn-name (format "my-vertico-indexed-insert-%s" num)))
      `(progn
         (defun ,(intern fn-name) ()
           ,(format "Execute `vertico-insert' with %s as a prefix argument" num)
           (interactive)
           (let ((current-prefix-arg '(,num))) (call-interactively #'vertico-insert)))
         (general-define-key
          :keymaps 'vertico-map
          ,(format "M-%s" num) #',(intern fn-name)))))
  (dolist (num (number-sequence 1 9)) (eval `(my-vertico-bind-indexed-insert-fun ,num))))

(use-package prescient                  ; Ordered but fuzzy command completion
  :custom
  (completion-styles '(prescient basic)  "Use prescient completion styles")
  (prescient-save-file (expand-file-name "prescient-save.el" my-state-dir)
                       "Put the save file in the right place"))

(use-package marginalia                 ; Minibuffer annotations
  :custom
  (marginalia-align 'right "Align margin annotations to the right")
  :config (marginalia-mode t))

(use-package all-the-icons-completion   ; Pretty icons in completion
  :demand t
  :after (marginalia all-the-icons)
  :hook (marginalia-mode . all-the-icons-completion-marginalia-setup)
  :config (all-the-icons-completion-mode t))

;; TODO helm-swoop bindings

(use-package consult                    ; Better interfaces for various menus
  ;; TODO helm-swoop replacement bindings
  :general
  ([remap switch-to-buffer] #'consult-buffer
   [remap switch-to-buffer-other-frame] #'consult-buffer-other-frame
   [remap switch-to-buffer-other-window] #'consult-buffer-other-window
   [remap bookmark-jump] #'consult-bookmark
   [remap project-switch-to-buffer] #'consult-project-buffer
   [remap imenu] #'consult-imenu
   [remap yank-pop] #'consult-yank-pop
   [remap apropos] #'consult-apropos
   [remap repeat-complex-command] #'consult-complex-command
   "M-/" #'consult-line
   "C-M-/" #'consult-line-multi)
  (minibuffer-local-map "M-s" #'consult-history)
  ('(motion visual normal)
   "gc" #'consult-imenu)
  ;; useful for default completion UI
  :hook (completion-list-mode . consult-preview-at-point-mode))

(use-package embark-consult
  :after (embark consult)
  :hook (embark-collect-mode . consult-preview-at-point-mode))


;;; Editing

(use-package delsel						; Built-in replace selected text when typing
  :config (delete-selection-mode t))

(use-package newcomment :straight nil   ; Built-in commenting and uncommenting
  :custom
  (comment-inline-offset 2           "Demand two spaces before inline comments")
  (comment-auto-fill-only-comments t "Only auto fill in comments")
  (kill-do-not-save-duplicates t     "Don't put duplicates in kill ring")
  :config
  ;; Toggle the active line comment first and only add an end-of-line comment if
  ;; the point is not at the end of the line.
  (define-advice comment-dwim (:around (orig-fun &rest args) comment-dwim-toggle)
    (if (and (not (region-active-p)) (not (eq (point) (line-end-position))))
        (comment-or-uncomment-region (line-beginning-position) (line-end-position))
      (apply orig-fun args))))

;; TODO anzu

;; TODO
(use-package elec-pair                  ; Built-in auto pairing quotes and parens
  :custom
  (electric-pair-pairs
   '((?\" . ?\")
     (?‘ . ?’)
     (?“ . ?”)
     (?[ . ?])
     (?{ . ?}))
   "Add extra characters to pair regardless of mode")
  :config (electric-pair-mode t))

(use-package company                    ; Auto complete
  :hook (prog-mode . company-mode)
  :general
  (company-active-map
   "TAB" #'company-select-next
   "<backtab>" #'company-select-previous
   "C-h" #'company-show-doc-buffer)
  :custom
  (company-idle-delay 0                   "Immediately attempt completion")
  (company-show-numbers t                 "Allow M-num selections")
  (company-tooltip-align-annotations nil  "Align annotations to the left border")
  (company-selection-wrap-around t        "Wrap when moving past the start/end of selection list")
  :config
  ;; don't autocomplete words in normal text
  (delete 'company-dabbrev company-backends))
(use-package emojify                    ; Display emojis :heart:
  :config (global-emojify-mode t))
(use-package ligature					; Ligatures with harfbuzz
  :config
  (ligature-set-ligatures
   'prog-mode
   '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
     ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
     "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
     "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
     "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
     "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
     "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
     "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
     ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
     "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
     "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
     "?=" "?." "??" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
     "\\\\" "://"))
  (global-ligature-mode t))
;;;; Undo
(use-package undo-fu :defer t)          ; Avoid redoing past an undo

(use-package undo-fu-session            ; Save undo sessions
  :after undo-fu
  :config (global-undo-fu-session-mode))

(use-package vundo						; Undo tree visualization
  :custom
  (vundo-compact-display t  "Take less on-screen space")
  :general
  ('normal "U" #'vundo)
  (vundo-mode-map
   "l" #'vundo-forward
   "<right>" #'vundo-forward
   "h" #'vundo-backward
   "<left>" #'vundo-backward
   "j" #'vundo-next
   "<down>" #'vundo-next
   "k" #'vundo-previous
   "<up>" #'vundo-next
   "g" #'vundo-stem-root
   "G" #'vundo-stem-end
   "q" #'vundo-quit
   "RET" #'vundo-confirm))




;;; Appearance
(use-package face-remap                 ; Built-in face remapping
  :hook
  (text-mode . variable-pitch)
  (after-init . text-scale-mode))


;;;; Pretty
(use-package solaire-mode               ; Distinguish editing buffers from other buffers
  :config (solaire-global-mode t))
(use-package all-the-icons              ; Pretty icons
  :if window-system)

(use-package doom-themes                ; Pretty colors
  :after all-the-icons
  :custom
  (doom-themes-enable-bold t    "Allow bold faces")
  (doom-themes-enable-italic t  "Allow italic faces")
  :config (load-theme 'doom-molokai t))

(use-package doom-modeline              ; Pretty modeline
  :after all-the-icons
  :custom
  (doom-modeline-buffer-encoding nil  "Don't show the buffer encoding")
  :custom-face
  (font-lock-keyword-face ((t (:slant italic))))
  :config (doom-modeline-mode t))


;;;; Faces
(use-package faces :straight nil		; Built-in face system
  :custom-face (default ((t (:family "VictorMono Nerd Font" :height 105)))))
(use-package paren                      ; Built-in paren highlighting
  :custom
  (show-paren-when-point-inside-paren t "Show parens when point inside parens")
  (show-paren-context-when-offscreen t  "Show paren context in the echo area if it starts outside of frame")
  :config (show-paren-mode t))

(use-package rainbow-delimiters         ; Colorize matching delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode               ; Color hex codes and color values
  :hook (prog-mode . rainbow-mode))

(use-package highlight-numbers          ; Colorize numbers
  :hook (prog-mode . highlight-numbers-mode))

;; TODO

(use-package hl-line                    ; Highlight the current line
  :hook (prog-mode . hl-line-mode))

(use-package rainbow-blocks :defer t)   ; Highlight blocks between parens

(use-package whitespace                 ; TODO investigate ws butler instead
  :hook (prog-mode . whitespace-mode)
  :custom
  (whitespace-line-column nil)
  (whitespace-style '(face trailing lines-tail))
  (whitespace-action '(auto-cleanup warn-if-read-only) "Try to fix whitespace and warn if read only"))

;; TODO window management
;; golden ratio?
;; Winner mode


;;; Files
(use-package files :straight nil        ; Built-in file management
  :custom
  (view-read-only t                    "Visit read-only files in view mode")
  (find-file-visit-truename t          "Make symlink buffer names use true name")
  (auto-save-file-name-transforms `((".*" ,auto-save-list-file-prefix t))
                                  "Keep autosave files away from my files")
  (lock-file-name-transforms `(("." ,(expand-file-name "locks" my-cache-dir)))
                             "Keep lock files away from my files")
  (backup-directory-alist `(("." . ,(expand-file-name "backups" my-data-dir)))
                          "Keep backup files away from my files")
  (backup-by-copying t                 "Always backup by copy")
  (version-control t                   "Add version numbers to file names")
  (delete-old-versions t               "Silently delete old backups")
  (kept-old-versions 1                 "Keep a single old version just in case")
  (kept-new-versions 2                 "Keep two new versions also just in case")
  (file-preserve-symlinks-on-save t    "Don't overwrite symlinks when saving")
  (revert-buffer-quick-short-answers t "Allow y-or-n-p for revert buffer rather than yes-or-no-p")
  (confirm-kill-emacs #'y-or-n-p       "Confirm before killing emacs")
  (require-final-newline 'visit-save   "Add a final newline on visit and on save")
  ;;(find-file-runs-dired nil       "No more accidentally opening dired!")
  :config (auto-save-visited-mode t))   ; actually save auto-saved files

(use-package savehist                   ; Persistent history between restarts
  :custom
  (savehist-file (expand-file-name "history" my-state-dir)  "Put history file in my state dir")
  (savehist-delete-duplicates t                             "Delete duplicates from the list")
  (savehist-save-minibuffer-history t                       "Save minibuffer history"))

(use-package uniquify :straight nil     ; Built-in distinguishing buffers by file path
  :custom
  (uniquify-buffer-name-style 'forward)
  (uniquify-trailing-separator-p t))

(use-package recentf					; Track recently opened files
  :custom
  (recentf-exclude '("COMMIT_EDITMSG") "Exclude commit messages from recentf list")
  (recentf-save-file (expand-file-name "recentf" my-state-dir)
                     "Put recentf history in the Right:tm: place")
  :config
  (recentf-mode t))
(use-package projectile :disabled t     ; Projects -- disabled for now to try out project.el
  :demand t
  :general
  (my-leader-map
   "p" projectile-command-map
   "P" #'projectile-find-file-dwim)
  :custom
  (projectile-cache-file (expand-file-name "projectile.cache" my-cache-dir)
                         "Projectile file in cache dir")
  (projectile-known-projects-file (expand-file-name "projectile.eld" my-state-dir)
                                  "Projectile state file in state dir")
  :config
  (projectile-mode t))
(use-package autorevert                 ; Auto-revert external modifications
  :custom
  (global-auto-revert-non-file-buffers t)
  :config (global-auto-revert-mode t))
(use-package project
  :general (my-leader-map "p" project-prefix-map))
;;; IDE shit
(use-package flycheck                   ; On-the-fly syntax checking
  :hook (prog-mode . flycheck-mode)
  :general (my-leader-map
            "c" #'flycheck-next-error
            "C" #'flycheck-previous-error)
  :custom
  (flycheck-indication-mode 'right-fringe))

(use-package flycheck-pos-tip           ; Show errors in a popup
  :after flycheck
  :hook (flycheck-mode . flycheck-pos-tip-mode))

(use-package flycheck-inline :disabled t ; Show errors inline after the error
  :after flycheck
  :hook (flycheck-mode . flycheck-inline-mode))

(use-package flycheck-status-emoji :disabled t) ; Maybe need work to get this to work with DOOM

(use-package lsp-mode                   ; Language servers!
  :hook
  (web-mode . lsp-deferred)
  (lsp-mode . lsp-enable-which-key-integration)
  (lsp-mode . lsp-modeline-code-actions-mode)
  (lsp-headerline-breadcrumb-mode . my-disable-which-func)
  :custom
  (lsp-modeline-diagnostics-scope :workspace))

(use-package lsp-ui)

;;; Tools

;;;; Version Control

(use-package vc-hooks :straight nil     ; Built-in version control
  :custom (vc-handled-backends '(Git) "Remove unnecessary VC backends; I only use Git"))

(use-package magit                      ; Magical git integration
  :general (my-leader-map "g" #'magit-status))

(use-package evil-magit :disabled t     ; TODO evil bindings?
  :after '(evil magit))

;;; Languages

;;;; Web
(use-package web-mode
  :mode ("\\.svelte?\\'" . web-mode)
  :custom
  (web-mode-markup-indent-offset 2 "Less indentation for markup")
  (web-mode-css-indent-offset 2    "Less indentation for styles")
  (web-mode-code-indent-offset 2   "Less indentation for code")
  (web-mode-style-padding 0        "No initial indentation for contents of <style> tags")
  (web-mode-script-padding 0       "No initial indentation for contents of <script> tags")
  (web-mode-enable-css-colorization t  "Show me colors for CSS hex values")
  (web-mode-enable-current-element-highlight t "Show me the current element")
  (web-mode-enable-current-column-highlight t  "Show me the current indentation level"))

(use-package tide                       ; Typescript IDE stuff
  :after (typescript-mode company flycheck)
  :hook
  ((typescript-mode . tide-setup)
   (web-mode . tide-setup)              ; I just use typescript for everything so always use tide
   (tide-mode . tide-hl-identifier-mode))
  :custom
  (tide-format-options
   '(:baseIndentSize 0 :indentSize 2 :tabSize 2 :convertTabsToSpaces t)
   "Less indentation"))
;;;; Emacs Lisp

(use-package eldoc                      ; Built-in documentation in the minibuffer
  :hook (prog-mode . eldoc-mode)
  :custom (eldoc-idle-delay 0.3 "Faster documentation"))

;; Faces for quoted lisp symbols and functions
(use-package highlight-quoted :hook (emacs-lisp-mode . highlight-quoted-mode))

;; TODO consider outshine mode
(use-package outline-mode :straight nil	 ; Built-in collapsible headers in code
  :hook (emacs-lisp-mode . outline-minor-mode)
  :general ('(normal motion) "TAB" #'outline-cycle))

(use-package outline-minor-faces		; Nicer faces for outline mode
  :after outline
  :hook (outline-minor-mode . outline-minor-faces-mode))

;; TODO Indent lists as lists, not as functions.
;; https://emacs.stackexchange.com/questions/10230/how-to-indent-keywords-aligned


;;; Footer

;; Treat comment headings and use-package declarations as headings but only
;; apply faces to the comment headings.

;; Local Variables:
;; flycheck-disabled-checkers: (emacs-lisp-checkdoc)
;; End:
